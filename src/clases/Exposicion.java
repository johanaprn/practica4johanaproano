package clases;

import java.time.LocalDate;

/**
 * Creacion de atributos, setter y getter,constructor y to String
 * 
 * @author Johana
 *
 */
public class Exposicion {
	private String nombreExposicion;
	private String nombreSala;
	private double precio;
	private LocalDate fechaInicio;
	private LocalDate fechaFinal;
	private int numeroObras;
	private boolean mayor16;
	private boolean compraInternet;
	private int numeroTrabajadores;

	// constructor
	public Exposicion(String nombreExposicion) {
		this.nombreExposicion = nombreExposicion;
	}

	// syg
	public String getNombreExposicion() {
		return nombreExposicion;
	}

	public void setNombreExposicion(String nombreExposicion) {
		this.nombreExposicion = nombreExposicion;
	}

	public String getNombreSala() {
		return nombreSala;
	}

	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public LocalDate getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public LocalDate getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(LocalDate fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public int getNumeroObras() {
		return numeroObras;
	}

	public void setNumeroObras(int numeroObras) {
		this.numeroObras = numeroObras;
	}

	public boolean isMayor16() {
		return mayor16;
	}

	public void setMayor16(boolean mayor16) {
		this.mayor16 = mayor16;
	}

	public boolean isCompraInternet() {
		return compraInternet;
	}

	public void setCompraInternet(boolean compraInternet) {
		this.compraInternet = compraInternet;
	}

	public int getNumeroTrabajadores() {
		return numeroTrabajadores;
	}

	public void setNumeroTrabajadores(int numeroTrabajadores) {
		this.numeroTrabajadores = numeroTrabajadores;
	}

	// to string
	@Override
	public String toString() {
		return "Exposicion : \nnombreExposicion=" + nombreExposicion + "\nnombreSala=" + nombreSala + "\nprecio="
				+ precio + "�" + "\nfechaInicio=" + fechaInicio + "\nfechaFinal=" + fechaFinal + "\nnumeroObras="
				+ numeroObras + "\nmayor16=" + mayor16 + "\ncompraInternet=" + compraInternet + "\nnumeroTrabajadores="
				+ numeroTrabajadores + "\n**********";
	}

}
