package clases;

import java.time.LocalDate;

public class Cuadro {
	/**
	 * Creacion de atributos de esta clase
	 */
	// Atributos
	private String nombreObra;
	private String autor;
	private LocalDate fechaCreacion;
	private String tecnica;
	private String soporte;
	private LocalDate fechaExposicion;
	private int numeroSerie;
	private double precio;
	private boolean vendido;

	/**
	 * Creacion del construcctor
	 * 
	 * @param nombreObra
	 */
	// Constructores
	public Cuadro(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	/**
	 * getters y setters
	 * 
	 * @return
	 */
	// Getters y setters
	public String getNombreObra() {
		return nombreObra;
	}

	public void setNombreObra(String nombreObra) {
		this.nombreObra = nombreObra;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getTecnica() {
		return tecnica;
	}

	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}

	public String getSoporte() {
		return soporte;
	}

	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}

	public LocalDate getFechaExposicion() {
		return fechaExposicion;
	}

	public void setFechaExposicion(LocalDate fechaExposicion) {
		this.fechaExposicion = fechaExposicion;
	}

	public int getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(int numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public boolean isVendido() {
		return vendido;
	}

	public void setVendido(boolean vendido) {
		this.vendido = vendido;
	}

	/**
	 * creacion toString
	 */
	// M�todo toString()
	@Override
	public String toString() {
		return "Cuadro: \nnombreObra: " + nombreObra + "\nautor: " + autor + "\nfechaCreacion: " + fechaCreacion
				+ "\ntecnica: " + tecnica + "\nsoporte: " + soporte + "\nfechaExposicion: " + fechaExposicion
				+ "\nnumeroSerie:" + numeroSerie + "\nprecio: " + precio + "�" + "\nvendido: " + vendido
				+ "\n���������������";
	}

}
