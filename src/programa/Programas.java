package programa;

import java.util.Scanner;

import clases.GaleriaObraArte;

/**
 * 
 * @author Johana
 *
 */
public class Programas {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		/**
		 * variables indicadoras del numero de cuadros y exposiciones que introduciremos
		 */
		int opcion;
		int numCuadros = 6;
		int numExposiciones = 4;
		GaleriaObraArte galeria = new GaleriaObraArte(numCuadros, numExposiciones);
		/**
		 * introduccion de datos de las dos clases
		 */
		// exposiciones
		galeria.altaExposicion("Gótico", "A23", 0.0, ("2020-03-01"), ("2025-02-01"), 15, false, false, 5);
		galeria.altaExposicion("Cubismo", "A24", 15.50, ("2022-02-01"), ("2025-02-01"), 15, false, true, 6);
		galeria.altaExposicion("Rococó", "A23", 7.00, ("2020-02-01"), ("2025-02-01"), 12, true, true, 4);

		// obras
		galeria.entradaObra("La noche estrellada", "Vincent van Gogh", ("1889-01-01"), "Oleo", "Lienzo", ("2020-01-02"),
				17546, 85.5, false);
		galeria.entradaObra("El beso", "Gustav Klimt", ("1908-01-01"), "Óleo", "Tela", ("2020-02-01"), 156856, 26.48,
				true);
		galeria.entradaObra("Los Nenúfares", "Claude Monet", ("1920-01-01"), "Óleo", "Tela", ("2017-03-01"), 1856,
				254.48, true);
		galeria.entradaObra("El grito", "Edvard Munch", ("1893-01-01"), "Óleo", "Cartón", ("2014-04-01"), 156856, 25.48,
				false);
		galeria.entradaObra("Leonardo Da Vinci", "Mona Lisa", ("1503-01-01"), "óleo", "Lienzo", ("2018-05-01"), 1856,
				254.48, true);

		/**
		 * Menú incial
		 */
		System.out.println("Bienvenido a nuestra galeria de arte");
		do {
			System.out.println("   ◥◣◥◤◢◤◢◣◥◣◥◤◢◤◢◣◥◣◥◤◢◤◢  ");
			System.out.println("   ◣◥        Elija una opción      ◣◥");
			System.out.println("   ◣◥         1-Exposiciones        ◣◥");
			System.out.println("   ◣◥                  2-Obras           ◣◥");
			System.out.println("   ◣◥ 	      3-Salir  	          ◣◥");
			System.out.println("   ◣◥◣◥◤◢◤◢◣◥◣◥◤◢◤◢◣◥◣◥◤◢◤ ");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				menuExposicion(9, galeria);
				break;

			case 2:
				menuObra(6, galeria);
				break;
			case 3:
				System.out.println("Adiós");
				break;
			default:
				System.out.println("Opción erronea. Intentelo de nuevo");
				break;
			}

		} while (opcion != 3);
	}

	// *************** M E N U * E X P O S I C I O N ********************

	/**
	 * Método con el menu de exposicición
	 * 
	 * @param opcion  nos permitira escoger entre las diferentes opciones del menú
	 * @param galeria es donde pasamos todos los datos de la clase GaleriaObraArte
	 */
	static void menuExposicion(int opcion, GaleriaObraArte galeria) {
		do {
			System.out.println("¿Qué deseea hacer?");
			System.out.println("**********************************************************");
			System.out.println("1-Iniciar una nueva exposición");
			System.out.println("2-Listar todas las exposiciones");
			System.out.println("3-Exposiciones que se pueden comprar en internet");
			System.out.println("4-Exposición con descuento");
			System.out.println("5-Volver al inicio ");

			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				// presentar una nueva exposicion
				System.out.println("**Introducir una nueva exposicion**");
				galeria.altaExposicion("Modernismo", "A23", 15.50, ("2020-02-01"), ("2020-02-01"), 15, false, true, 2);

				// listar para ver la exposicion introducida
				galeria.listarExposiciones();
				break;

			case 2:
				// buscar exposiciones disponibles
				System.out.println("**Todas las exposciciones:");
				galeria.listarExposiciones();
				break;

			case 3:
				System.out.println("**Exposiciones que puedan ser compradas a través de internet**");
				galeria.buscarCompraInternet(true);
				break;

			case 4:
				System.out.println("**Exposición en la que se aplica el descuento**");
				galeria.descuentoEntrada("Cubismo", 0.8, 15.50);
				break;

			case 5:
				System.out.println("Vuelve al incio");
				break;

			default:
				System.out.println("Opción incorrecta. Inténtelo de nuevo");
				break;

			}
		} while (opcion != 5);

	}

	/**
	 * Metodo menú de las obras
	 * 
	 * @param opcion  que utilizaremos para indicar la opción que vamos a escoger
	 * @param galeria es donde pasamos todos los datos de la clase GaleriaObraArte y
	 *                que utilizaremos
	 */

	// *************** M E N U * O B R A S ********************
	static void menuObra(int opcion, GaleriaObraArte galeria) {
		do {
			System.out.println("¿Qué desea hacer ?");
			System.out.println("1-Dar de alta una obra ");
			System.out.println("2-Buscar alguna obra");
			System.out.println("3-Eliminar alguna obra ");
			System.out.println("4-Mostrar todas las obras");
			System.out.println("5-Mostrar obras por autor");
			System.out.println("6-Salir");

			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				// obras dadas de alta en la galeria

				System.out.println("··DAR DE ALTA UNA OBRA NUEVA··");
				galeria.entradaObra("Los Girasoles", "Vincent van Gogh", ("1962-01-01"), "Óleo", "Lienzo",
						("1908-01-01"), 12729, 1785.5, false);

				// listar obras
				galeria.listarObras();
				break;
			case 2:
				// buscar obras
				// el cuadro que vamos a buscar es "El beso"
				System.out.println("··BUSCAR OBRA··");
				System.out.println(galeria.buscarCuadros("El beso"));

				break;
			case 3:
				// eliminar obra
				System.out.println("··ELIMINAR OBRA·· ");
				galeria.eliminarObra("Los Nenúfares");
				galeria.listarObras();
				break;
			case 4:
				// cambiarobra
				System.out.println("··CAMBIAR OBRA··");
				galeria.cambiarObra("El beso", 15.4);
				galeria.listarObras();
				break;
			case 5:
				// listar por autor
				System.out.println("··OBRAS DEL MISMO AUTOR··");
				galeria.listarporautor("Vincent van Gogh");
				break;
			case 6:
				System.out.println("Que tenga un buen día. Gracias por visitar nuestra página");
				break;
			default:
				System.out.println("△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽");
				System.out.println("Opción incorrecta. Por favor inténtelo de nuevo");
				System.out.println("Recuerde que solo tiene 6 opciones");
				System.out.println("△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽△▽");
				break;
			}

		} while (opcion != 6);

	}
}
