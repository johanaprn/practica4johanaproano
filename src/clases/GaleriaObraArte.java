package clases;

import java.time.LocalDate;

public class GaleriaObraArte {

	// Atributos
	private Exposicion[] exposiciones;
	private Cuadro[] cuadros;

	// Constructores
	/**
	 * Constructores
	 * 
	 * @param numCuadros      nos indirar� cuantos cuatros podemos introducir
	 * @param numExposiciones paramentro en el diremos cuantas exposiciones podr�n
	 *                        ser introducidas
	 */
	public GaleriaObraArte(int numCuadros, int numExposiciones) {
		this.cuadros = new Cuadro[numCuadros];
		this.exposiciones = new Exposicion[numExposiciones];
	}

	// ***********************Exposiciones*****************************

	// Metodo alta
	/**
	 * M�todo que utilizaremos para dar de alta nuevas obras
	 * 
	 * @param nombreExposicion
	 * @param nombreSala
	 * @param precio
	 * @param fechaInicio
	 * @param fechaFinal
	 * @param numeroObras
	 * @param mayor16
	 * @param compraInternet
	 * @param numeroTrabajadores
	 */
	public void altaExposicion(String nombreExposicion, String nombreSala, double precio, String fechaInicio,
			String fechaFinal, int numeroObras, boolean mayor16, boolean compraInternet, int numeroTrabajadores) {
		for (int i = 0; i < exposiciones.length; i++) {
			if (exposiciones[i] == null) {
				exposiciones[i] = new Exposicion(nombreExposicion);
				exposiciones[i].setNombreSala(nombreSala);
				exposiciones[i].setPrecio(precio);
				exposiciones[i].setFechaInicio(LocalDate.parse(fechaInicio));
				exposiciones[i].setFechaFinal(LocalDate.parse(fechaFinal));
				exposiciones[i].setNumeroObras(numeroObras);
				exposiciones[i].setMayor16(mayor16);
				exposiciones[i].setCompraInternet(compraInternet);
				exposiciones[i].setNumeroTrabajadores(numeroTrabajadores);
				break;
			}
		}

	}

	/**
	 * M�todo con el que buscamos aquellas obras en las que las entradas se pueden
	 * comprar por internet
	 * 
	 * @param compraInternet
	 */
	// metodo si se puede comprar por internet
	public void buscarCompraInternet(boolean compraInternet) {
		for (int i = 0; i < exposiciones.length; i++) {
			if (exposiciones[i] != null) {
				if (exposiciones[i].isCompraInternet() == compraInternet) {
					System.out.println(exposiciones[i]);
				}
			}
		}
	}

	/**
	 * M�todo con el que veremos todas las exposciones disponibles
	 */
	// listar exposiciones
	public void listarExposiciones() {
		for (int i = 0; i < exposiciones.length; i++) {
			if (exposiciones[i] != null) {
				System.out.println(exposiciones[i]);
			}
		}
	}

	/**
	 * M�todo que servira para calcular el descuento en la obra seleccionada
	 * 
	 * @param nombreExposicion
	 * @param descuento
	 * @param precio
	 */
	// colocar descuento en la entrada
	public void descuentoEntrada(String nombreExposicion, double descuento, double precio) {

		for (int i = 0; i < exposiciones.length; i++) {
			if (exposiciones[i] != null) {
				if (exposiciones[i].getNombreExposicion().equals(nombreExposicion)) {
					System.out.println(nombreExposicion + " : " + precio * descuento + "�");
				}
			}
		}
	}

	// ***************************Obras********************************
	/**
	 * M�todo para dar de alta una obra nueva
	 * 
	 * @param nombreObra
	 * @param autor
	 * @param fechaCreacion
	 * @param tecnica
	 * @param soporte
	 * @param fechaExposicion
	 * @param numeroSerie
	 * @param precio
	 * @param vendido
	 */
	// M�todo alta
	public void entradaObra(String nombreObra, String autor, String fechaCreacion, String tecnica, String soporte,
			String fechaExposicion, int numeroSerie, double precio, boolean vendido) {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] == null) {
				cuadros[i] = new Cuadro(nombreObra);
				cuadros[i].setAutor(autor);
				cuadros[i].setTecnica(tecnica);
				cuadros[i].setSoporte(soporte);
				cuadros[i].setFechaCreacion(LocalDate.parse(fechaCreacion));
				cuadros[i].setFechaExposicion(LocalDate.parse(fechaExposicion));
				cuadros[i].setNumeroSerie(numeroSerie);
				cuadros[i].setPrecio(precio);
				cuadros[i].setVendido(vendido);
				break;
			}

		}
	}

	/**
	 * M�todo para buscar obras por su nombre
	 * 
	 * @param nombreObra
	 * @return nos devolver� el nombre de la obra que hayamos introducido
	 */
	// M�todo buscar
	public Cuadro buscarCuadros(String nombreObra) {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] != null) {
				if (cuadros[i].getNombreObra().equals(nombreObra)) {
					return cuadros[i];
				}

			}
		}
		return null;
	}

	/**
	 * M�todo para eliminar obras por su nombre
	 * 
	 * @param nombreObra
	 */
	// M�todo eliminar
	public void eliminarObra(String nombreObra) {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] != null) {
				if (cuadros[i].getNombreObra().equals(nombreObra)) {
					cuadros[i] = null;
				}
			}
		}
	}

	/**
	 * M�todo para mostrar todas las obras que esten dadas de alta en ese momento
	 */
	// M�todo listar general
	public void listarObras() {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] != null) {
				System.out.println(cuadros[i]);
			}
		}
	}

	/**
	 * M�todo para poder cambiar el precio de la obra inciada
	 * 
	 * @param nombreObra elegiremos la obra a la que cambiemos el precio
	 * @param precio     nos devuelve el precio ya cambiado
	 */
	// M�todo cambiar
	public void cambiarObra(String nombreObra, double precio) {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] != null) {

				if (cuadros[i].getNombreObra() == nombreObra) {
					cuadros[i].setPrecio(precio);
				}
			}
		}

	}

	/**
	 * M�todo con el listaremos todos las obras que tengan el mismo autor
	 * 
	 * @param autor para indicar por lo que queremos buscar
	 */
	// M�todo listar por alg�n atributo de la Clase1
	public void listarporautor(String autor) {
		for (int i = 0; i < cuadros.length; i++) {
			if (cuadros[i] != null) {
				if (cuadros[i].getAutor().equals(autor)) {
					System.out.println(cuadros[i]);
				}
			}
		}

	}

}
